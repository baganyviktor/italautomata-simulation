Felhasználói dokumentáció az italautomata szimulációhoz</br></br>

Készítette:
Bagány Viktor
G8V82U
baganyviktor@gmail.com

Feladat:
Egy ételautomata szimulálása a zsák absztrakt típus megvalósításával. A tesztelés megkönnyítésére legyen lehetőség "a gép belsejébe látni", lehessen kezdeti készletet megadni mind készpénzből, mind termékből, és lehessen különféle vásárlásokat lebonyolítani. Két zsák szerepel a feladatban: termékek zsákja, pénzes zsák.

Használat:
A program Windows 10 1803 (build 17134.407) alatt készült és tesztelődött Visual Studio 2017 környezetben, Visual C++ compiler.
A lefordított program a Debug/AlgoZH2_Automata.exe -vel indítható. Elindulás után megjelenik egy állapot, ami megmutatja a felhasználónak a termék- illetve a kassza kezdeti állapotát. Innen egy gomb lenyomásával lehet az automata használatához eljutni.
Az ablak kezdeti állapota a menü. A menüben a megfelelő szám beírásával, majd enter lenyomással választhatunk menüpontot.
    • Az első menüpont a pénzbedobás, az összeg megadása. Az automata elfogad 5, 10, 20, 50, 100, 500, 1000, 2000 érméket illetve bankjegyeket.
    • A vásárlás a 2-es menüpontból érhető el, megfelelő összeg esetén. Összegtöbblet esetén a vásárlás végén automatikusan megtörténik a pénzvisszaadás.
    • A 3-as menüpontban a “karbantartó” ellenőrizheti a kassza illetve a termékek adatbázisát.
    • A 4-es menüpont a bedobott összeg visszaadására szolgál, amennyiben a felhasználó mégsem vásárol, illetve ha az automatában pénz ragad.

Hiba esetén a program közli a hibát a felhasználóval.
A program fájlból tölti fel a kasszát is és a termékeket is.

A kassza fájl (kassza.txt) tagolása szóközzel elválasztva:
Bankjegy darabszám

A termék fájl (keszlet.txt) tagolása:
Termék neve, ára darabszám