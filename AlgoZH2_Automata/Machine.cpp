#include "pch.h"
#include "Machine.h"


Machine::Machine()
{
	moneyinTray = 0;
}

void Machine::FillCassa(int banknote, int amount) //Szerintem egy�rtelm�
{
	if (!ValidBanknote(banknote)) throw Exceptions::NotValidBanknote;
	if (amount < 1) throw Exceptions::NotValidAmount;
	for (int i = 0; i < amount; i++)
	{
		Cassa.add(banknote);
	}
}

void Machine::FillItem(Item item, int amount) //Item t�pust v�r, nulla darabbal exception
{
	if (item.name() == "") throw Exceptions::ItemNameNotSpecified;
	if (amount < 1) throw Exceptions::NotValidAmount;
	for (int i = 0; i < amount; i++)
	{
		Items.add(item);
	}
}

void Machine::DebugItems()
{
	Items.WriteOut(std::cout); //Ki�rja a term�kek list�j�t a standard outputra
}

void Machine::DebugCassa()
{
	Cassa.WriteOut(std::cout);
}

void Machine::BuyItem(Item item)
{
	if (!Items.find(item)) throw Exceptions::ItemNotFound;
	int price = GetItem(item).price();
	if (moneyinTray < price) throw Exceptions::NotEnoughMoneyInTray;
	int moneyinTraysave = moneyinTray;
	moneyinTray -= price;
	Items.remove(item);
	std::cout << "\nTermek kiadva.\n";
	try
	{
		Cashback(moneyinTray);
	}
	catch (Exceptions e) // Tov�bbdobjuk az exception-t
	{
		if (e == Exceptions::CashBackProblem)
		{
			throw Exceptions::CashBackProblem;
		}
		else if (e == Exceptions::NotEnoughMoneyInCassa)
		{
			throw Exceptions::NotEnoughMoneyInCassa;
		}
	}
}

void Machine::ThrowMoneyIn(int banknote)
{
	if (!ValidBanknote(banknote)) throw Exceptions::NotValidBanknote;
	moneyinTray += banknote;
	Cassa.add(banknote);
}

int Machine::GetTrayMoney()
{
	return moneyinTray;
}

void Machine::Cashback(int amount)
{
	std::vector<int> last;

	int limit = moneyinTray;
	int money = limit;
	MultiSet<int> tempcassa(Cassa); // M�solunk egy kassz�t, mert nem az �les kassz�n akarunk pr�b�lkozni a backtrack-el (B�r ott is lehetne)
	if (rCashBack(tempcassa, limit, money, last, 1)) //Elind�tjuk a visszal�p�ses p�nzvisszaad�s folyamat�t, ha sikeres ki�rjuk az eredm�nyt
	{
		std::cout << "Visszajaro:";
		for (int i = 0; i < (int)last.size(); i++)
		{
			Cassa.remove(last[i]);
			moneyinTray -= last[i];
			std::cout << " " << last[i];
		}
		std::cout << "\n";
	}

	if (Cassa.isEmpty()) throw Machine::Exceptions::NotEnoughMoneyInCassa;
	if(!Cassa.isEmpty() && moneyinTray != 0) throw Machine::Exceptions::CashBackProblem;

}

bool Machine::rCashBack(MultiSet<int>& m,int limit, int money, std::vector<int>& l, int melyseg = 1)
{
	if (money == 0) return true; // Akkor k�sz vagyunk, meg�llunk
	if (limit < 0) return false; //Akkor nem tudtunk vissza adni, jelezz�k a sikertelens�get
	if (money < limit) limit = money; // Na az�rt ne legyen m�r a limit nagyobb mint a visszaadand� p�nz mennyis�ge
	int max = 0;
	while (money != 0 && !m.isEmpty() && (max = m.GetMax(limit)) > 0) //Megpr�b�lunk vissza adni MOH� algoritmussal
	{
		//if (max == 0) return false;
		l.push_back(max); //K�zben t�ltj�k a "stack"-et, ha vissza kellene l�pni
		money -= max;
		m.remove(max);
		if (money < limit) limit = money;
	}
	if (money == 0)
	{
		//l.clear(); //Nem �r�j�k ki a stack-et mert tartalmazza a kidoband� �rm�ket
		return true; //Ez volt a c�l, bej�tt a moh� algoritmus
	}
	else // nem j�tt be a moh�, indul a backtrack
	{ 
		int lastlast;
		for (int i = 0; i < melyseg; i++) //vissza l�p�nk m�lys�gnyit
		{
			if (l.size() == 0) return false;
			lastlast = l.back();
			m.add(lastlast);
			l.pop_back();
			money += lastlast;
		}
		if (l.size() == 0) return false;
		int last = l.back(); //meg m�g egyet, k�l�n kellett szednem a limit meghat�roz�sa miatt
		m.add(last);
		l.pop_back();
		money += last;
		
		return rCashBack(m, (limit + last) - lastlast, money, l, ++melyseg);
	}
}

bool Machine::ValidBanknote(int banknote)
{
	switch (banknote)
	{
		case 5: return true; break;
		case 10: return true; break;
		case 20: return true; break;
		case 50: return true; break;
		case 100: return true; break;
		case 200: return true; break;
		case 500: return true; break;
		case 1000: return true; break;
		case 2000: return true; break;
		default: return false; break;
	}
}

Item Machine::GetItem(Item i)
{
	return Items.GetItem(i);
}

Machine::~Machine()
{
}
