#pragma once

#include <iostream>

template <class T>
struct Data
{
	T data;
	int counter;
};

template <class T>
class Node
{
public:
	Node();
	~Node();
	Data<T> key;
	Node* next;
	static int alloc_counter;
};

template <class T>
int Node<T>::alloc_counter = 0;

template <class T>
class MultiSet
{
public:
	MultiSet();
	MultiSet(const MultiSet<T>& m);
	~MultiSet();
	void add(T item);
	Node<T>* find(T item);
	T GetItem(T i);
	void remove(T item);
	void WriteOut(std::ostream & out);
	bool isEmpty() { return size == 0; }
	T GetMax();
	T GetMax(T limit);
	enum Exceptions { NotIn, NotEnoughMemory, };
private:
	Node<T>* head;
	int size;
};

template<class T>
inline Node<T>::Node()
{
	next = nullptr;
	alloc_counter++;
}

template<class T>
inline Node<T>::~Node()
{
	//if (next) 
	{
		alloc_counter--;
		//std::cout << "[DEBUG]: Unallocating memory (" << this << "), remaining: "<<alloc_counter<<"\n";
		delete next; 
	}
}
