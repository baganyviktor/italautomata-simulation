// main.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <conio.h>
#include "Machine.h"
#include <fstream>
#include <sstream>

using namespace std;

int main()
{
	try
	{
		// Létrehozom az italautomata objektumot, feltöltöm a kasszát valid összegekkel, feltöltöm a termékeket valid termékekkel
		Machine italautomata;

		ifstream input;
		input.open("kassza.txt");
		int cash, amount;
		if (input.is_open())
		{
			while (input >> cash >> amount)
			{
				italautomata.FillCassa(cash, amount);
			}
			input.close();
		}

		else cout << "Unable to open file";


		ifstream input2;
		input2.open("keszlet.txt");
		string line;
		string name; int amount2, price, id;
		id = 1;
		if (input2.is_open())
		{
			while (getline(input2, name, ','))
			{
				getline(input2, line, '\n');
				stringstream ss(line);
				ss >> price >> amount2;
				italautomata.FillItem(Item(name, price, id), amount2);
				++id;
			}
			input2.close();
		}

		else cout << "Unable to open file";


		cout << "A kassza keszletenek feltoltese: \n";
		italautomata.DebugCassa();

		cout << "\nA termekek keszletenek feltoltese: \n";
		italautomata.DebugItems();

		system("pause"); // olvashatóság miatt

		enum control { INMENU, BUYING, MONEYIN, DEBUG }; // Menüállapotok kezelése
		control state = INMENU;
		while (true)
		{
			if(state == INMENU)
			{
				system("cls");
				cout << "Penz bedobashoz ird be: 1. \nVasarlashoz: 2.\nDebughoz: 3\nPenz visszaadas: 4\n\n";
				cout << "Hitel: " << italautomata.GetTrayMoney() << endl; // object.GetTrayMoney() -- a bedobott, el nem költött kredit
				char pressed; cin >> pressed;
				switch (pressed)
				{
					case '1': state = MONEYIN; break;
					case '2': state = BUYING; break;
					case '3': state = DEBUG; break;
					case '4': 
						try 
						{
							italautomata.Cashback(italautomata.GetTrayMoney());
						}
						catch (Machine::Exceptions e)
						{
							if (e == Machine::Exceptions::NotEnoughMoneyInTray)
							{
								cout << "[ERROR]: Nincs megfelelo osszeg!\n";
							}
							else if (e == Machine::Exceptions::CashBackProblem)
							{
								cout << "[ERROR]: Nincs megfelelo osszeg a kasszaban! Kerem hivja az ugyfelszolgalatot.\n";
							}
							else cout << "[ERROR]: Hiba tortent.\n";
						}
						system("pause"); 
						break;
				}
			}
			else if (state == BUYING)
			{
				system("cls");
				cout << "Termekek:\n";
				italautomata.DebugItems();
				cout << "\nAdd meg a termek kodjat: ";
				try
				{
					int num = 0; cin >> num;
					italautomata.BuyItem(Item(num)); //object.BuyItem(ITEM) -- kap egy item-et ami lehet valid úgy, hogy
													 // megfelelő a termék id-ja VAGY ( megfelelő a neve ÉS az ára )
					cout << "\nSikeres vasarlas.\n";
				}
				catch (Machine::Exceptions e)
				{
					if (e == Machine::Exceptions::NotEnoughMoneyInTray)
					{
						cout << "[ERROR]: Nincs megfelelo osszeg!\n";
					}
					else if (e == Machine::Exceptions::CashBackProblem)
					{
						cout << "[ERROR]: Nincs megfelelo osszeg a kasszaban! Kerem hivja az ugyfelszolgalatot.\n";
					}
					else cout << "[ERROR]: Hiba tortent.\n";
				}
				system("pause");
				state = INMENU;
			}
			else if (state == MONEYIN)
			{
				cout << "Adj meg egy ermet vagy bankjegyet: ";
				int money = 0; cin >> money;
				try
				{
					italautomata.ThrowMoneyIn(money); // pénzt helyez el a Tray-en. Csak valid érméket fogad el, ellenkező esetben Exception
				}
				catch (Machine::Exceptions e)
				{
					if (e == Machine::Exceptions::NotValidBanknote)
					{
						cout << "[ERROR]: Nincs ilyen erme vagy bankjegy!\n";
						system("pause");
					}
					else cout << "[ERROR]: Hiba tortent.\n";
				}
				state = INMENU;
			}
			else if (state == DEBUG)
			{
				system("cls");
				cout << "Keszlet debug: \n";
				italautomata.DebugItems();
				cout << "\nKassza debug: \n";
				italautomata.DebugCassa();
				cout << "\n";
				system("pause");
				state = INMENU;
			}
		}

		cout << "\nEnd of Process. Press any key to stop..\n";

	}
	catch (const Machine::Exceptions e)
	{
		switch (e)
		{
		case Machine::Exceptions::NotValidBanknote:
			cout << "Not Valid Banknote\n"; break;
		case Machine::Exceptions::ItemNameNotSpecified:
			cout << "Item name not specified!\n"; break;
		case Machine::Exceptions::NotValidAmount:
			cout << "Not valid amount for filling machine\n"; break;
		default:
			break;
		}
	}
}