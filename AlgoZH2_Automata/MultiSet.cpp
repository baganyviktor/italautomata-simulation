#include "pch.h"
#include <string>
#include <iostream>
#include "MultiSet.h"
#include "Machine.h" //A template miatt musz�j

template <class T>
MultiSet<T>::MultiSet()
{
	try
	{
		head = new Node<T>;
	}
	catch (const std::exception&)
	{
		throw MultiSet::Exceptions::NotEnoughMemory;
	}
	size = 0;
}

template <class T>
MultiSet<T>::MultiSet(const MultiSet<T>& m)
{
	head = new Node<T>;
	Node<T>* cur = m.head->next;
	while (cur != nullptr)
	{
		for (int i = 0; i < cur->key.counter; i++)
		{
			this->add(cur->key.data);
		}
		cur = cur->next;
	}
}

template<class T>
MultiSet<T>::~MultiSet()
{
	delete head; //rekurz�van t�rli az eg�sz multisetet.
}

template<class T>
void MultiSet<T>::add(T item)
{
	Node<T>* p = find(item);
	if (p == nullptr)
	{
		Node<T>* created;
		try
		{
			created = new Node<T>;
		}
		catch (const std::exception&)
		{
			throw Exceptions::NotEnoughMemory;
		}
		created->key.data = item;
		created->next = head->next;
		head->next = created;
		created->key.counter++;
		size++;
	}
	else
	{
		p->key.counter++;
	}
}

template<class T>
Node<T>* MultiSet<T>::find(T item)
{
	Node<T>* current = head->next;
	while (current != nullptr && item != current->key.data)
	{
		current = current->next;
	}
	return current;
}

template<class T>
T MultiSet<T>::GetItem(T i)
{
	Node<T>* temp = find(i);
	if (temp == nullptr) throw Exceptions::NotIn;
	return temp->key.data;
}

template<class T>
void MultiSet<T>::remove(T item)
{
	Node<T>* p = find(item);
	if (p == nullptr) throw Exceptions::NotIn;
	if (p->key.counter > 1)
	{
		p->key.counter--;
	}
	else
	{
		Node<T>* current = head->next;
		if (head->next == p)
		{
			head->next = p->next;
			p->next = nullptr;
			delete p;
		}
		else
		{
			while (current->next != p)
			{
				current = current->next;
			}
			current->next = p->next;
			p->next = nullptr;
			delete p;
		}
	}
}

template<class T>
void MultiSet<T>::WriteOut(std::ostream& out)
{
	Node<T>* current = head->next;
	while (current)
	{
		out << current->key.data << " : " << current->key.counter << "\n";
		current = current->next;
	}
}

template<class T>
T MultiSet<T>::GetMax()
{
	Node<T>* max = head;
	Node<T>* temp = head->next;
	while (temp)
	{
		if (temp->key.data > max->key.data)
		{
			max = temp;
		}
		temp = temp->next;
	}
	return max->key.data;
}

template<class T>
T MultiSet<T>::GetMax(T limit)
{
	Node<T>* max = head;
	Node<T>* temp = head->next;
	while (temp)
	{
		if (temp->key.data > max->key.data && temp->key.data <= limit)
		{
			max = temp;
		}
		temp = temp->next;
	}
	return max->key.data;
}

template class MultiSet<Item>;
template class MultiSet<int>;
