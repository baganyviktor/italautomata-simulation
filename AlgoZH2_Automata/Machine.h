#pragma once

#include <string>
#include <vector>
#include "MultiSet.h"

class Item
{
public:
	Item() { itemid = 0; itemname = ""; value = 0; }
	Item(std::string _itemname, int price, int id) { itemname = _itemname; value = price; itemid = id; }
	Item(int id) { itemid = id; itemname = ""; value = 0; }
	std::string name() { return itemname; }
	int price() { return value; }
	friend std::ostream& operator<<(std::ostream& o, const Item a) { o << "("<< a.itemid <<") "<< a.itemname << " : " << a.value; return o; }
	friend bool operator==(const Item a, const Item b) { return (a.itemname == b.itemname && a.value == b.value) || (a.itemid == b.itemid);}
	friend bool operator!=(const Item a, const Item b) { return (a.itemname != b.itemname || a.value != b.value) && (a.itemid != b.itemid); }
	operator int() { return value; };

private:
	int itemid;
	std::string itemname;
	int value;
};

class Machine
{
public:
	Machine();
	void FillCassa(int banknote, int amount);
	void FillItem(Item item, int amount);
	void DebugItems();
	void DebugCassa();
	void BuyItem(Item item);
	void ThrowMoneyIn(int banknote);
	int GetTrayMoney();
	void Cashback(int amount);
	enum Exceptions { NotValidBanknote, NotValidAmount, ItemNameNotSpecified, ItemNotFound, NotEnoughMoneyInTray, NotEnoughMoneyInCassa,
		CashBackProblem };
	~Machine();

private:
	bool rCashBack(MultiSet<int>& m, int limit, int money, std::vector<int>& l, int melyseg);
	bool ValidBanknote(int banknote);
	Item GetItem(Item i);

	MultiSet<Item> Items;
	MultiSet<int> Cassa;
	int moneyinTray;
};

